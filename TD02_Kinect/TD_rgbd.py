#!/usr/bin/env python
import numpy as np
import cv2
import open3d as o3d
import os
#from write_and_read_sync_imgs import open_shot
# Note: Be careful, "depths" given by Kinect are not actually "depths" dut disparities. In common language, these terms are often considered the same.

# %%


def open_shot(n, fromdir='./shots'):
    # open a shot according to its number
    depth = cv2.imread(
        '{}/{:02d}_depth.png'.format(fromdir, n), cv2.IMREAD_ANYDEPTH)
    depth_pretty = cv2.imread('{}/{:02d}_depth_pretty.png'.format(fromdir, n))
    bgr = cv2.imread('{}/{:02d}_rgb.png'.format(fromdir, n))
    return depth, depth_pretty, bgr

# %%


def raw_depth_to_meters(raw_disp):
    # get distances in meter from depth given by Kinect
    # Kinect computes the depth, but outputs a value coded a specific way in [[0;2047]] on 11 bits
    # informations on https://openkinect.org/wiki/Imaging_Information
    m_depth = np.zeros_like(raw_disp, dtype=float)
    # raw_disp must be transformed, some explanations in https://wiki.ros.org/kinect_calibration/technical
    b = 0.075  # baseline in meters
    f = 580.0  # focal length in pixels
    doff = 1090.0  # disparity offset in pixels

    m_depth = b * f / (1/8 * (doff - raw_disp))

    return m_depth

# %%


def xyz_from_depth_meters(m_depth):
    # Compute 3d points from depth map
    f = 580.0  # focal length of IR camera in pixels
    cx = m_depth.shape[1] / 2  # center of image for x
    cy = m_depth.shape[0] / 2  # center of image for y

    ind = np.indices(m_depth.shape)
    x = (ind[1] - cx) * m_depth / f
    y = (ind[0] - cy) * m_depth / f
    z = m_depth

    # Store in xyz format
    xyz = np.zeros((m_depth.shape[0], m_depth.shape[1], 3), dtype=float)
    xyz[:, :, 0] = x
    xyz[:, :, 1] = y
    xyz[:, :, 2] = z

    # Flatten xyz to a list of points
    xyz = xyz.reshape((-1, 3))

    return xyz

# %%


def color_of_xyz(xyz, bgr_img):
    # Homogeous version of xyz
    xyz_h = np.concatenate((xyz.T, np.ones((1, xyz.shape[0]))), axis=0)

    # Back-project 3D points in rgb image and get closest color (no interpolation)
    R = np.identity(3, dtype=float)
    # translation from IR camera to RGB camera
    T = np.array([[-0.025], [0.], [0.]])
    transfo = np.vstack((np.hstack((R, T)), np.array([0., 0., 0., 1.])))
    xyz_rgb_h = np.matmul(transfo, xyz_h)

    cx = bgr_img.shape[1] / 2  # center of image for x
    cy = bgr_img.shape[0] / 2  # center of image for y
    f = 525.  # Focal of the rgb camera

    # Calibration matrix of the rgb camera
    IC = np.array(
        [[f, 0, cx, 0],
         [0, f, cy, 0],
         [0, 0, 1, 0]]
    )

    # Projection of 3D points in rgb image in homogeneous coordinates
    uv_rgb_h = np.matmul(IC, xyz_rgb_h)

    # Get color of the closest point for each 3D point
    rgb_colors = np.zeros((xyz.shape[0], 3), dtype=float)
    for i in range(xyz.shape[0]):
        u = uv_rgb_h[0, i] / uv_rgb_h[2, i]
        v = uv_rgb_h[1, i] / uv_rgb_h[2, i]
        rgb_colors[i] = bgr_img[int(v), int(u)]

    return rgb_colors


# %%
def main(shot):
    path = os.path.join(os.getcwd(), 'shots')
    # open RGB and D data of the shot
    depth, depth_pretty, bgr = open_shot(shot, path)
    cv2.imshow('Depth', depth_pretty)  # display depth
    cv2.imshow('RGB', bgr)  # display RGB

    # depth map in meters
    m_depth = raw_depth_to_meters(depth)

    # 3D point cloud visualization
    xyz = xyz_from_depth_meters(m_depth)  # no-colored version
    # Pass xyz to Open3D.o3d.geometry.PointCloud before to visualize
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(xyz)

    # add color information
    rgb_colors = color_of_xyz(xyz, bgr)
    pcd.colors = o3d.utility.Vector3dVector(rgb_colors/255.)
    # prepare 3D visualizer
    v = o3d.visualization.Visualizer()
    v.create_window()
    # Show axes
    v.add_geometry(o3d.geometry.TriangleMesh.create_coordinate_frame())
    v.add_geometry(pcd)

    while 1:
        v.poll_events()
        v.update_renderer()  # show 3D point cloud
        if cv2.waitKey(10) == 27:
            cv2.destroyAllWindows()
            v.destroy_window()
            break


# %%
if __name__ == "__main__":
    cv2.namedWindow('Depth')
    cv2.namedWindow('RGB')
    print('Press ESC in window to stop')

    shot = 10  # shot number to open
    main(shot)
