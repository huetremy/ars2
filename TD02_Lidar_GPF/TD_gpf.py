#!/usr/bin/env python
# Ground plane fitting (GPF) algorithm from 3D lidar scan shot in the street.
# This code extracts the ground assuming it is a single plane.
# It is based on the work described in "Fast Segmentation of 3D Point Clouds: A Paradigm on LiDAR Data for Autonomous Vehicle Applications", D. Zermas, I. Izzat and N. Papanikolopoulos, 2017.
# The algorithm is well-explained in the reference, PDF is given in this archive.
#
# A finer extension could be done by cutting the point clouds into independant portions among car direction.

import numpy as np
import open3d as o3d

# %%


def naive_ground_extractor(ptc, n_lpr):
    # First, try to make a basic and naive ground extractor, as a reference to compare with GPF
    # simplest ground extractor: takes points with lowest Z
    # returns ids of ground
    # ptc: array of points
    # n_lpr: number of lowest Z points to keep for ground

    # TODO

    return ground_ids


# %%
# GPF: from paper "Fast Segmentation of 3D Point Clouds: A Paradigm on LiDAR Data for Autonomous Vehicle Applications"
def gpf_extractinitialseeds(ptc, n_lpr, thresh_seeds=0.5):
    # GPF, extract initial seeds
    # returns ids of seeds
    # ptc: array of points
    # n_lpr: number of lowest Z points to keep for initialization
    # thresh_seeds: threshold distance to keep seeds from initial ground plane issued from lowest Z points (in meters)

    # TODO

    return seeds_ids

# %%


def gpf_estimateplane(ptc, plane_ids):
    # GPF, plane estimation from points ids and ptc
    # computes covariance matrix of plane points distribution, then SVD to get normal vector of the plane
    # returns ground plane equation defined by its normal (a,b,c) and d
    # ptc: array of points
    # plane_ids: ids of plane points

    # TODO

    return normal, d

# %%


def gpf_refinement(ptc, seeds_ids, thresh_dist=0.2, n_iter=5):
    # GPF, main loop to refine ground plane estimation
    # returns ids of ground
    # ptc: array of points
    # seeds_ids: ids of seeds points
    # thresh_dist: distance threshold to keep points from ground plane (in meters)
    # n_iter: number of iterations

    # TODO

    return ground_ids


# %%
def main(pcd_file):
    # read the pointcloud
    pointcloud = o3d.io.read_point_cloud(pcd_file)
    # to work with point cloud, needs to pass to a numpy array
    pt = np.asarray(pointcloud.points)
    # pt has the xyz data as a numpy array, column per column
    # print(pt)
    print(pt.shape)

    # ptc point cloud as an array with x, y, z, class (0: non-ground, 1: ground) / class is used to plot points with distinct colors
    ptc = np.hstack((pt, np.zeros((pt.shape[0], 1))))
    print(ptc.shape)

    # test naive ground extractor
    #ground_ids = naive_ground_extractor(ptc, 10000)

    # Or, GPF method
    # get initial seeds
    #seeds_ids = gpf_extractinitialseeds(ptc, 20000, 0.2)
    # test ground only from initial seeds
    #ground_ids = seeds_ids
    # or, continue to complete method, with ground detection refinement
    #ground_ids = gpf_refinement(ptc, seeds_ids, 0.2, 5)

    # update ground class info in ptc
    # ptc[ground_ids[:],3] = 1 # 1 for ground points
    print(np.count_nonzero(ptc[:, 3] == 1))  # number of ground points
    print(np.count_nonzero(ptc[:, 3] == 0))  # number of non-ground points

    # show with colors according to class
    colors = np.zeros((pt.shape[0], 3))  # init color points to black
    colors[ptc[:, 3] == 1, :] = [1., 0., 0.]  # print ground points in red
    pointcloud.colors = o3d.utility.Vector3dVector(
        colors)  # add colors to the open3d pointcloud
    o3d.visualization.draw_geometries(
        [pointcloud, o3d.geometry.TriangleMesh.create_coordinate_frame()])  # includes axes


# %%
if __name__ == "__main__":
    pcd_file = './pointclouds/1504941056.807104000.pcd'
    main(pcd_file)
